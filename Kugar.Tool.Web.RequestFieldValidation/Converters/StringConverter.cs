﻿using System;
using Kugar.Core.ExtMethod;

namespace Kugar.Tool.Web.RequestFieldValidation.Converters
{
    internal class StringConverter : IRequestValueConverter
    {
        public object ConvertTo<T>(string inputValue, string format, object defaultValue)
        {
            return inputValue.ToStringEx((string)defaultValue);
        }

        public bool CanConvert(Type valueType)
        {
            return valueType == typeof(string);
        }
    }
}