﻿using System;
using System.Globalization;

namespace Kugar.Tool.Web.RequestFieldValidation.Converters
{
    internal class DateTimeConverter : IRequestValueConverter
    {
        public object ConvertTo<T>(string inputValue, string format, object defaultValue)
        {
            if (DateTime.TryParseExact(inputValue, format, CultureInfo.GetCultureInfo("zh-cn"), DateTimeStyles.AllowWhiteSpaces, out var dt))
            {
                return dt;
            }
            else
            {
                return defaultValue;
            }
        }

        public bool CanConvert(Type valueType)
        {
            return valueType == typeof(ValueType) || valueType == typeof(DateTime) || valueType == typeof(DateTime?);
        }
    }
}