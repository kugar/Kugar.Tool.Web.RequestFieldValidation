﻿using System;

namespace Kugar.Tool.Web.RequestFieldValidation.Converters
{
    public interface IRequestValueConverter
    {
        object ConvertTo<T>(string inputValue, string format, object defaultValue);

        bool CanConvert(Type valueType);
    }
}