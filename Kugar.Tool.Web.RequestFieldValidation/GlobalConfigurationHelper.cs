﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace Kugar.Tool.Web.RequestFieldValidation
{
    public static class GlobalConfigurationHelper
    {
        public static void UseRequestFieldValidate(this IServiceCollection services)
        {
            
            services.AddScoped<RequestFieldCollection>();
        }
    }
}
