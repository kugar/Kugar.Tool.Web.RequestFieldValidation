﻿该类库用于针对Request进行校验,与默认的校验的区别在于,,,不需要单独写用于校验的类,可直接使用字段的name进行数据校验


启用Request请求校验方式的时候,需要使用一下配置

using Kugar.UI.Web.AdminLTE.RequestValidate;

public void ConfigureServices(IServiceCollection services)
{
	services.UseRequestFieldValidate();  

}



使用方式:

原有方式:
var age1 = Request.GetInt("age");

if (age1>=20)
{
    ModelState.AddModelError("age","必须大于20");
}

if (age1==10)
{
    ModelState.AddModelError("age","必须等于10");
}

if (age1<10)
{
    ModelState.AddModelError("age","必须小于10");
}

转换方式

//单字段校验
int age = Request.GetField<int>("age", 20)
                .GTE(20.0, "必须大于20")
                .EQ(10)
                .LT(10)
                .AddToModelState(ModelState) //不调用该函数,不会进行校验及错误输出
                .Value();  //添加校验之后,直接返回值

//数组字段校验
var s = Request.GetArrayValueField("sss", Array.Empty<double>())
    .WithElement((index, item) => item.GT(20).EqualToField(""))  //列表中每个值进行校验
    .AddToModelState(ModelState) //不调用该函数,不会进行校验及错误输出
    .Value();

