﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Kugar.Tool.Web.RequestFieldValidation
{
    public interface IRequestField
    {
        string StringValue { get; }

        IEnumerable<IRule> Rules { get; }

        bool IsValid { get; }

        HttpRequest Request { get; }

        void AddRule(IRule rule);

        string FieldName { get; }

        string Title { get; }

        string Format { get; }
    }

    public interface IRequestField<T> : IRequestField
    {
        T Value();
        
    }

    public interface IRequestArrayField: IRequestField
    {
        //IEnumerable<IRule> ElementRules { get; }
    }

    public interface IRequestArrayField<out TArray, TItem> : IRequestArrayField where TArray : IEnumerable<TItem>
    {
        TItem[] Value();



        IRequestArrayElementField<TItem>[] ElementFields { get; }

        /// <summary>
        /// 重新设置值,可用于对数组值进行再次过滤处理,处理后的值,将替换原有的值,如手动过滤空值等操作
        /// </summary>
        /// <param name="castFunc">转换函数</param>
        /// <returns></returns>
        IRequestArrayField<TArray, TItem> ResetValues(Func<IEnumerable<TItem>, IEnumerable<TItem>> castFunc);

        /// <summary>
        /// 用于针对每个子元素进行数据校验
        /// </summary>
        /// <param name="checkFunc"></param>
        /// <returns></returns>
        IRequestArrayField<TArray, TItem> WithElement(ArrayFieldCheckElement<TItem> checkFunc);

        IRequestArrayField<TArray, TItem> WithElement(ArrayFieldCheckElementWithIndex<TItem> checkFunc);

        /// <summary>
        /// 用于在有需要的时候,根据子元素的序号,生成子元素的字段名
        /// </summary>
        /// <param name="nameBuilder"></param>
        /// <returns></returns>
        IRequestArrayField<TArray, TItem> WithElementNameBuilder(Func<int, string> nameBuilder);
    }

    public interface IRequestArrayElementField<T> : IRequestField<T>
    {
        IRequestArrayElementField<T> ElementFieldName(string fieldName);
    }
}