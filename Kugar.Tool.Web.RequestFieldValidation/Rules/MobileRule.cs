﻿using System;
using System.Collections.Generic;
using System.Text;
using Kugar.Core.ExtMethod;

namespace Kugar.Tool.Web.RequestFieldValidation.Rules
{
    public class MobileRule:RuleBase
    {
        public MobileRule(string errorMsg) : base(errorMsg)
        {
        }

        protected override string BuildDefaultErrorMessage(IRequestField field)
        {
            return Localizer["{0}必须输入手机号码", field.Title];// $"{field.Title}必须输入手机号码";
        }

        protected override bool ValidateField(IRequestField field)
        {
            var f = (IRequestField<string>)field;

            var value = f.Value();

            if (string.IsNullOrWhiteSpace(value))
            {
                return true;
            }

            return GeneralRegex.IsMobilePhone(value);
        }
    }
}
