﻿using System;
using System.Collections.Generic;
using System.Text;
using Kugar.Core.ExtMethod;

namespace Kugar.Tool.Web.RequestFieldValidation.Rules
{
    public class ValueInRule<TInputValue> : RuleBase
    {
        private TInputValue[] _checkedValues = null;

        public ValueInRule(string errorMsg,TInputValue[] values) : base(errorMsg)
        {
            _checkedValues = values;
        }

        protected override string BuildDefaultErrorMessage(IRequestField field)
        {
            return Localizer["{0}必须在指定范围内", field.Title];// $"{field.Title}必须在指定范围内";
        }

        public TInputValue[] Values => _checkedValues;

        protected override bool ValidateField(IRequestField field)
        {
            var cv = RequestFieldServices.ConvertTo<TInputValue>(field.StringValue, field.Format, default);


            foreach (var checkedValue in _checkedValues)
            {
                if (cv.SafeEquals(checkedValue))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
