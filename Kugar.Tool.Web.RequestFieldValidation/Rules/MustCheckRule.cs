﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Routing;

namespace Kugar.Tool.Web.RequestFieldValidation.Rules
{
    /// <summary>
    /// 指定表达式必须为true的检查
    /// </summary>
    /// <typeparam name="TInputValue"></typeparam>
    public class MustCheckRule<TInputValue>:RuleBase
    {
        private Func<TInputValue, bool> _func = null;

        internal MustCheckRule(Func<TInputValue,bool> func,string errorMsg):base(errorMsg)
        {
            _func = func;
        }

        protected override string BuildDefaultErrorMessage(IRequestField field)
        {
            return $"{field.Title}不能为空";
        }

        protected override bool ValidateField(IRequestField field)
        {
            return _func((field as IRequestField<TInputValue>).Value());
        }
    }
}
