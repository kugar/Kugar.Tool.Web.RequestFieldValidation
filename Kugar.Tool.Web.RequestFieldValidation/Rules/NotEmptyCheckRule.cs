﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kugar.Tool.Web.RequestFieldValidation.Rules
{
    public class NotEmptyCheckRule: RuleBase
    {
        public NotEmptyCheckRule(string errorMsg):base(errorMsg)
        {
        }

        //public override string DefaultErrorMessage
        //{
        //    get
        //    {
        //        return $"不能为空";
        //    }
        //}

        protected override string BuildDefaultErrorMessage(IRequestField field)
        {
            return Localizer["{0}必填", field.Title]; // $"{field.Title}不能为空";
        }

        protected override bool ValidateField(IRequestField field)
        {
            return !string.IsNullOrWhiteSpace(field.StringValue);
        }
    }
}
