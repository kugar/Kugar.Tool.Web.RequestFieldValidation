﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Kugar.Tool.Web.RequestFieldValidation.Rules
{
    public class RegexRule: RuleBase
    {
        private Regex _regex = null;

        public RegexRule(string regex, RegexOptions options, string errorMsg) : base(errorMsg)
        {
            _regex=new Regex(regex, options);
        }

        protected override string BuildDefaultErrorMessage(IRequestField field)
        {
            return $"{field.FieldName}不符合规则";
        }

        protected override bool ValidateField(IRequestField field)
        {
            throw new NotImplementedException();
        }
    }
}
