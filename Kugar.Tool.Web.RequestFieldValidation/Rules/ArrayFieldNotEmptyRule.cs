﻿using System;
using System.Collections.Generic;
using System.Text;
using Kugar.Core.ExtMethod;

namespace Kugar.Tool.Web.RequestFieldValidation.Rules
{
    public class ArrayFieldNotEmptyRule<TInputValue> : RuleBase
    {
        internal ArrayFieldNotEmptyRule(string errorMsg):base(errorMsg)
        {
        }

        //public override string DefaultErrorMessage => "不能为空";

        protected override string BuildDefaultErrorMessage(IRequestField field)
        {
            return Localizer["{0}不能为空", ((IRequestField<TInputValue>)field).Title];//$"{((IRequestField<TInputValue>)field).Title}不能为空";
        }

        protected override bool ValidateField(IRequestField field)
        {
            var f = (IRequestArrayField<IEnumerable<TInputValue>,TInputValue>) field;

            return f.Value().HasData();
        }
    }
}
