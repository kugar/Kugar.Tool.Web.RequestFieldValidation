﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Kugar.Tool.Web.RequestFieldValidation.Rules
{
    /// <summary>
    /// 客户端通过url发送数据进行远程校验,必须返回ResultReturn类似结构
    /// </summary>
    public class ClientRemoteCheckRule:RuleBase
    {
        public ClientRemoteCheckRule(string url,string method="get",string[] withFieldNames=null,string errorMsg="") : base(errorMsg)
        {
            Url = url;
            Method = method;
            WithFieldNames = withFieldNames;
        }

        public string Url { get; }

        public string Method { get; }

        public string[] WithFieldNames { get; }
        
        protected override string BuildDefaultErrorMessage(IRequestField field)
        {
            return "";

        }

        protected override bool ValidateField(IRequestField field)
        {
            return true;
        }
    }
}
