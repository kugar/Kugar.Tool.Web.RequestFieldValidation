﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Routing;

namespace Kugar.Tool.Web.RequestFieldValidation.Rules
{
    public class ExistsCheckRule:RuleBase
    {
        public ExistsCheckRule(string errorMsg) : base(errorMsg)
        {
        }

        //public override string DefaultErrorMessage
        //{
        //    get
        //    {
        //        return $"不能为空";
        //    }
        //}

        protected override string BuildDefaultErrorMessage(IRequestField field)
        {
            return $"{field.Title}必须存在";
        }

        protected override bool ValidateField(IRequestField field)
        {
            if (field.Request.Form.ContainsKey(field.FieldName))
            {
                return true;
            }

            if (field.Request.Query.ContainsKey(field.FieldName))
            {
                return true;
            }

            if (field.Request.HttpContext.GetRouteData().Values.ContainsKey(field.FieldName))
            {
                return true;
            }

            return false;
        }
    }
}
