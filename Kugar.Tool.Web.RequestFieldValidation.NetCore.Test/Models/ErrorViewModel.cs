using System;

namespace Kugar.Tool.Web.RequestFieldValidation.NetCore.Test.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}