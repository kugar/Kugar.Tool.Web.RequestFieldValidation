﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Kugar.Core.BaseStruct;
using Kugar.Core.ExtMethod;
using Kugar.Core.Web;
using Microsoft.AspNetCore.Mvc;
using Kugar.Tool.Web.RequestFieldValidation.NetCore.Test.Models;
using Kugar.Tool.Web.RequestFieldValidation.Rules;
using Kugar.UI.Web.AdminLTE.Helpers;
using Newtonsoft.Json.Linq;

namespace Kugar.Tool.Web.RequestFieldValidation.NetCore.Test.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            //Request.BuildClientValidate<int>("age").GTE(20.0);
            //Request.BuildClientValidate<string>("age")
            //    .GTE(20.0, "必须大于20")
            //    .EQ(10)
            //    .LT(10);

            var v=20;

            var field=WrapperToField.From(v,"v");

            return View();
        }

        [ActionName("Index"), HttpPost]
        public IActionResult IndexPost()
        {
            CultureInfo.CurrentUICulture=new CultureInfo("en");
            //var Text = Request.GetField("Text", "").NotEmpty("必填项").AddToModelState(ModelState).Value();
            //var Text = Request.GetField<decimal?>("Text", null).Must(x => x.HasValue, "必填").AddToModelState(ModelState).Value();
            //var Text = Request.GetField<bool?>("Text", null).Value();
            //var Text = Request.GetField("Text", "").Exists("Text不存在").AddToModelState(ModelState).Value();

            //var Date = Request.GetField("Date", DateTime.MinValue, "yyyy-MM-dd HH:mm:ss").Must(x => x != DateTime.MinValue, "please select").EqualToField("StartDt",errorMsg: "==StartDt").AddToModelState(ModelState).Value();

            //var StartDt = Request.GetField<DateTime?>("StartDt", null, "yyyy-MM-dd HH:mm:ss").Must(x => x.HasValue, "StartDt必填").Must(x => x > DateTime.Now, "StartDt>now").Must(x => x < DateTime.Now.AddDays(5), "StartDt<5d+now").AddToModelState(ModelState).Value();
            //var EndDt = Request.GetField<DateTime?>("EndDt", null, "yyyy-MM-dd HH:mm:ss").Must(x => x.HasValue, "EndDt必填").Must(x => x > StartDt, "EndDt>StartDt").Must(x => x < DateTime.Now.AddDays(10), "EndDt<10d+now").AddToModelState(ModelState).Value();

            //var StartVal = Request.GetField("StartVal", 0).GT(15, "StartVal>15").LT(20, "StartVal<20").EQ(18, "StartVal=18").AddToModelState(ModelState).Value();
            //var EndVal = Request.GetField("EndVal", 0).GTE(150, "EndVal>=150").LTE(200, "EndVal<=200").AddToModelState(ModelState).Value();

            //var TextButton = Request.GetField("TextButton", "").NotEmpty("必填").AddToModelState(ModelState).Value();
            //var TextButtonID = Request.GetField<int?>("TextButtonID", null).Must(x => x.HasValue, "ID!=null").AddToModelState(ModelState).Value();

            //var Select = Request.GetField("Select", -1).Must(x => x != -1, "请选择").AddToModelState(ModelState).Value();

            //var CheckBox = Request.GetArrayValueField("CheckBox", Array.Empty<int>()).NotEmpty("必选项").WithElement((index, item) => item.GT(1, ">1")).AddToModelState(ModelState).Value();

            //var Radio = Request.GetField(".Radio", -1).Must(x => x != -1, "请选择").GTE_When(1, Select == -1).AddToModelState(ModelState).Value();

            //var pcd = Request.GetField("pcd", "").NotEmpty("选择省市区").AddToModelState(ModelState).Value();
            //var addr = Request.GetField("addr", "").NotEmpty("填写地址").AddToModelState(ModelState).Value();

            //var File = Request.GetField("File", "").NotEmpty("please upload").AddToModelState(ModelState).Value();
            //var Image = Request.GetField("Image", "").NotEmpty("please upload").AddToModelState(ModelState).Value();

            //var MultiFile = Request.GetArrayValueField("MultiFile", Array.Empty<string>()).NotEmpty("please upload").AddToModelState(ModelState).Value();
            //var MultiImage = Request.GetArrayValueField("MultiImage", Array.Empty<string>()).NotEmpty("please upload").AddToModelState(ModelState).Value();

            //var Textarea = Request.GetField("Textarea", "").MinLength(5, "len>=5").MaxLength(10, "len<=10").EQ("aaaaaa", "value=aaaaaa").AddToModelState(ModelState).Value();
            //var SummerNote = Request.GetField("SummerNote", "").Length(20,10,"len:10~20").AddToModelState(ModelState).Value();
            var t = Request.GetField<int?>("Text", -1).GTE(0).Value().GetValueOrDefault();
            var keys = Request.Form.Keys.Where(x => x.StartsWith("dd_"));
            foreach (var key in keys)
            {
                var index = key.Split('_')[1];
                var cc = Request.GetField($"cc_{index}", "").Must(x => !string.IsNullOrWhiteSpace(x), $"requiredcc{index}").AddToModelState(ModelState).Value();
                var rr = Request.GetField($"rr_{index}", "").Must(x => !string.IsNullOrWhiteSpace(x), $"requiredrr{index}").AddToModelState(ModelState).Value();
                var tt = Request.GetField($"tt_{index}", "").Must(x => !string.IsNullOrWhiteSpace(x), $"requiredtt{index}").AddToModelState(ModelState).Value();
                var ss = Request.GetField($"ss_{index}", -1).Must(x => x != -1, $"requiredss{index}").AddToModelState(ModelState).Value();
                var tb = Request.GetField($"tb_{index}", "").Must(x => !string.IsNullOrWhiteSpace(x), $"requiredtb{index}").AddToModelState(ModelState).Value();
                var tbID = Request.GetField($"tbID_{index}", "").Must(x => !string.IsNullOrWhiteSpace(x), $"requiredtbID{index}").AddToModelState(ModelState).Value();
                var ii = Request.GetField($"ii_{index}", "").Must(x => !string.IsNullOrWhiteSpace(x), $"requiredii{index}").AddToModelState(ModelState).Value();
                var dd = Request.GetField<DateTime?>($"dd_{index}", null, "yyyy-MM-dd").Must(x => x.HasValue, $"requireddd{index}").AddToModelState(ModelState).Value();
            }

            Request.AddToModelState(ModelState);

            return View();
        }

        public ResultReturn Upload()
        {
            return new SuccessResultReturn("http://pic.58pic.com/58pic/17/19/64/18Q58PICmXU_1024.jpg");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
