﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Kugar.UI.Web.AdminLTE.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Kugar.Core.Web;
using Microsoft.AspNetCore.Localization;

namespace Kugar.Tool.Web.RequestFieldValidation.NetCore.Test
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.Configure<RequestLocalizationOptions>(options => {
                options.DefaultRequestCulture = new RequestCulture("zn-cn");  //默认的语言
            });
            services.AddLocalization(); //注册相应Service　　

            services.UseAdminLTE();

            services.UseRequestFieldValidate();

            services.AddHttpContextAccessor();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            var support = new List<CultureInfo>()
            {
                new CultureInfo("zh-cn"),  //注册多种语言,具体可以查看http://www.lingoes.net/zh/translator/langcode.htm找对应
                new CultureInfo("en")
            };
            app.UseRequestLocalization(x =>
            {
                x.SetDefaultCulture("zh-cn");
                x.SupportedCultures = support;  //设置支持的语言
                x.SupportedUICultures = support;  //设置UI语言,这里有个很大的坑,如果不设置该属性,在Action中,CultureInfo.CurrentCulture返回的是正确的语言,但是在CultureInfo.CurrentUICulture返回的是默认语言
                x.RequestCultureProviders.Insert(0,new CookieRequestCultureProvider());
                //x.AddInitialRequestCultureProvider(new AcceptLanguageHeaderRequestCultureProvider());  //设置判断当前语言的方式,我项目中是使用了Accept-Language 的header值作为判断
            });

            app.UseAdminLTEStaticFile();

            app.UseStaticHttpContext();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
